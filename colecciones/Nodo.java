package colecciones;


/**
 * Write a description of class Nodo here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
 class Nodo<T>
{
    // instance variables - replace the example below with your own
    
    private T info; 
    private Nodo<T> sig; 
    

    /**
     * Constructor for Ts of class Nodo
     */
    Nodo()
    {
       // initialise instance variables
    
    }

    
    
    Nodo(T info, Nodo<T> sig)
    {   
        this.info=info;
        this.sig=sig;
        
    }
    
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie info*/
      T getInfo(){
        return this.info;
    }//end method getInfo

    /**SET Method Propertie info*/
     void setInfo(T info){
        this.info = info;
    }//end method setInfo

    /**GET Method Propertie sig*/
     colecciones.Nodo<T> getSig(){
        return this.sig;
    }//end method getSig

    /**SET Method Propertie sig*/
     void setSig(colecciones.Nodo<T> sig){
        this.sig = sig;
    }//end method setSig

    //End GetterSetterExtension Source Code
//!
}
